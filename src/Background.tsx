import React from "react";
import "./main.css";

const Background = (props: { children: React.ReactNode }) => {
  return <div className={"background-container"}>{props.children}</div>;
};

export default Background;
