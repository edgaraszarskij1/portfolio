import React from "react";
import Background from "./Background";
import Resume from "./resume/Resume";
import About from "./about/About";
import Sidebar from "./sidebar/Sidebar";
import Home from "./home/Home";
import Portfolio from "./portfolio/Portfolio";
import "./sidebar/screen.css";
import { Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className={"full-container"}>
      <Sidebar />
      <Background>
        <Switch>
          <Route Route path="/" exact component={Home} />
          <Route Route path="/resume" component={Resume} />
          <Route Route path="/about" component={About} />
          <Route Route path="/portfolio" component={Portfolio} />
        </Switch>
      </Background>
    </div>
  );
}

export default App;
