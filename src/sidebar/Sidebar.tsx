import React, { useState } from "react";
import "./screen.css";
import SideBarComp from "./SidebarComp";
import MobileSidebar from "./MobileSidebar";

const Sidebar = (): JSX.Element => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const onOpenSidebar = () => {
    setIsOpen((prevState) => !prevState);
  };
  const onCloseSidebar = () => {
    setIsOpen(false);
  };
  return (
    <>
      <SideBarComp />
      <div
        className={"sidebar-button-ui-container"}
        onClick={() => onOpenSidebar()}
      >
        <div className={"sidebar-button-ui"}></div>
      </div>
      {isOpen && <MobileSidebar onClose={onCloseSidebar} />}
    </>
  );
};

export default Sidebar;
