import React from "react";
import "./screen.css";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import { NavLink } from "react-router-dom";
import { me } from "../portfolio/images";
import HomeIcon from "@material-ui/icons/Home";
import InfoIcon from "@material-ui/icons/Info";
import WorkIcon from "@material-ui/icons/Work";
import FolderIcon from "@material-ui/icons/Folder";
const SidebarComp = (): JSX.Element => {
  return (
    <div className={"sidebar-container"}>
      <div className={"sidebar"}>
        <div className={"picture-sidebar-container"}>
          {" "}
          {me && <AccountCircleIcon className={"icon-size"} />}
        </div>
        <ul className={"menu-ul"}>
          <div>
            <li>
              <NavLink
                className={"menu-li"}
                activeClassName=" active"
                exact
                to="/"
              >
                <span className={"test"}>
                  {<HomeIcon className={"sidebar-icons-ui"} />} HOME
                </span>
              </NavLink>
            </li>
            <li>
              <NavLink
                className={"menu-li"}
                activeClassName=" active"
                to="/about"
              >
                <span className={"test"}>
                  {<InfoIcon className={"sidebar-icons-ui"} />} ABOUT
                </span>
              </NavLink>
            </li>
            <li>
              <NavLink
                className={"menu-li"}
                activeClassName=" active"
                to="/resume"
              >
                <span className={"test"}>
                  {<WorkIcon className={"sidebar-icons-ui"} />} RESUME
                </span>
              </NavLink>
            </li>
            <li>
              <NavLink
                className={"menu-li"}
                activeClassName=" active"
                to="/portfolio"
              >
                <span className={"test"}>
                  {<FolderIcon className={"sidebar-icons-ui"} />} PORTFOLIO
                </span>
              </NavLink>
            </li>
          </div>
        </ul>
        <div className={"grid-last-ele"}></div>
      </div>
    </div>
  );
};

export default SidebarComp;
