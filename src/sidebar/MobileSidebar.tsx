import React from "react";
import "./screen.css";

import { NavLink } from "react-router-dom";

const MobileSideBar = (props: { onClose: () => void }): JSX.Element => {
  return (
    <div className={"small-sidebar"} onClick={() => props.onClose()}>
      <ul className={"menu-ul"}>
        <li>
          <NavLink className={"menu-li"} activeClassName="active" exact to="/">
            <span className={"test"}>HOME</span>
          </NavLink>
        </li>
        <li>
          <NavLink className={"menu-li"} activeClassName="active" to="/about">
            <span className={"test"}>ABOUT</span>
          </NavLink>
        </li>
        <li>
          <NavLink className={"menu-li"} activeClassName="active" to="/resume">
            <span className={"test"}>RESUME</span>
          </NavLink>
        </li>
        <li>
          <NavLink
            className={"menu-li"}
            activeClassName=" active"
            to="/portfolio"
          >
            <span className={"test"}>PORTFOLIO</span>
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default MobileSideBar;
