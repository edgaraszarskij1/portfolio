import React from "react";
import "./home.css";
import Link from '@material-ui/core/Link';
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import GitHubIcon from "@material-ui/icons/GitHub";

const Home = (): JSX.Element => {
  return (
    <div className={"parent-container"}>
      <div className={"center-home"}>
        <div className={"center-style"}>
          <h1 className={"home-style"}>Hello, I'm </h1>
          <h1 className={"sub-title home-style"}>Edgaras Žarskij.</h1>
        </div>
        <div className={"center-style"}>
          <h1 className={"home-style"}>I'm </h1>
          <h1 className={"sub-title home-style"}>
            Stubborn, Curious, Thorough, Empathetic, Friendly, Unyielding.
          </h1>
        </div>
        <Link href="https://www.linkedin.com/in/edgaras-%C5%BEarskij-53336a200/">
          <LinkedInIcon className={"mi-icons"} />
        </Link>
        <Link href="https://gitlab.com/edgaraszarskij1/">
          <GitHubIcon className={"mi-git-icon"} />
        </Link>
      </div>
    </div>
  );
};

export default Home;
