import React from "react";
import CloseIcon from "@material-ui/icons/Close";

interface ProjectProps {
  onCloseComponent: (e: React.MouseEvent<SVGElement>) => void;
  Component: any;
}

const ProjectComponent = (props: ProjectProps): JSX.Element => {
  return (
    <div className={"shadow"}>
      <CloseIcon onClick={props.onCloseComponent} className={"close-button"} />
      <div className={"component-container"}>
        <props.Component />
      </div>
    </div>
  );
};

export default ProjectComponent;
