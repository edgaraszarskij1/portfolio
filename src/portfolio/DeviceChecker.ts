import * as Bowser from "bowser";

const browser = Bowser.getParser(window.navigator.userAgent);

export const isValidBrowser = browser.satisfies({
  mobile: {
    safari: ">=9",
    "android browser": ">3.10",
    chrome: ">20.1.1432",
  },
});
