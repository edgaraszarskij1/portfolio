import React from "react";
import "./portfolio.css";
import ZoomInIcon from "@material-ui/icons/ZoomIn";
import OpenInNewIcon from "@material-ui/icons/OpenInNew";
import { PCBrowserProps } from "./BrowserProps";

const PcBrowser = (props: PCBrowserProps): JSX.Element => {
  return (
    <div
      onMouseLeave={props.handleMouseLeave}
      style={{ height: "100%", width: "100%" }}
    >
      {!props.isLoaded ? (
        <div className={"box"} onMouseEnter={props.handleMouseEnter}>
          <div
            className={
              props.isHovered
                ? "pic-hover"
                : props.isUnhovered
                ? "pic-unhover"
                : ""
            }
          >
            <div className={"center-icon"}>
              <ZoomInIcon
                className={
                  props.isHovered
                    ? "icon"
                    : props.isUnhovered
                    ? "icon-unhover"
                    : "hide-icons"
                }
                onClick={props.onOpen}
              />
              <OpenInNewIcon
                className={
                  props.isHovered
                    ? "icon"
                    : props.isUnhovered
                    ? "icon-unhover"
                    : "hide-icons"
                }
                onClick={props.onOpenComponent}
              />
            </div>
          </div>
          {<img src={props.pic} alt={"typing"} className={"small-pic-size"} />}
          <h3 className={"port-title"} onClick={props.onOpenComponent}>
            {props.title}
          </h3>
        </div>
      ) : (
        <div className={"gray-bg"}></div>
      )}
    </div>
  );
};

export default PcBrowser;
