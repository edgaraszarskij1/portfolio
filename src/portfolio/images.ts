import typing from "../images/typing.png";
import kapinynas from "../images/kapinynas.jpg";
import tictactoe from "../images/tictactoe.jpg";
import slidingPuzzle from "../images/slidingPuzzle.jpg";
import memoryPuzzle from "../images/memoryPuzzle.jpg";
import brackets from "../images/brackets.jpg";
import armory from "../images/armory.png";
import me from "../images/me.jpg";

export {
  typing,
  kapinynas,
  tictactoe,
  slidingPuzzle,
  memoryPuzzle,
  brackets,
  armory,
  me,
};
