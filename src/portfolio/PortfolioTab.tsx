import React, { useState } from "react";
import "./portfolio.css";
import ProjectComponent from "./ProjectComponent";
import { isValidBrowser } from "./DeviceChecker";
import { PortfolioProps } from "./PortfolioInterface";
import BigPicture from "./BigPicture";
import PcBrowser from "./PCBrowser";
import MobileBrowser from "./MobileBrowser";

const PortoflioTab = (props: PortfolioProps): JSX.Element => {
  const [isHovered, setIsHovered] = useState(false);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [isUnhovered, setIsUnhovered] = useState<boolean>(false);
  const [isComponentOpen, setIsComponentOpen] = useState<boolean>(false);
  const [isLoaded, setIsLoaded] = useState<boolean>(true);

  const imageToLoad = new Image();
  imageToLoad.src = props.pic;
  imageToLoad.onload = () => {
    setIsLoaded(false);
  };

  const onOpen = (): void => {
    setIsOpen(true);
    setIsHovered(false);
    setIsUnhovered(true);
  };

  const onClose = (): void => {
    setIsOpen(false);
    setIsHovered(false);
    setIsUnhovered(true);
  };

  const onOpenComponent = (): void => {
    setIsComponentOpen(true);
    setIsHovered(false);
    setIsUnhovered(true);
  };

  const onCloseComponent = (): void => {
    setIsComponentOpen(false);
    setIsHovered(false);
    setIsUnhovered(true);
  };

  const handleMouseEnter = () => {
    if (!isHovered) {
      setIsHovered(true);
      setIsUnhovered(false);
    }
  };
  const handleMouseLeave = () => {
    if (isHovered) {
      setIsHovered(false);
      setIsUnhovered(true);
    }
  };

  return (
    <div>
      {isOpen && <BigPicture onClose={onClose} pic={props.pic} />}
      {isComponentOpen && (
        <ProjectComponent
          onCloseComponent={onCloseComponent}
          Component={props.Component}
        />
      )}
      {!isValidBrowser ? (
        <PcBrowser
          handleMouseLeave={handleMouseLeave}
          handleMouseEnter={handleMouseEnter}
          isHovered={isHovered}
          isUnhovered={isUnhovered}
          title={props.title}
          isLoaded={isLoaded}
          onOpen={onOpen}
          onOpenComponent={onOpenComponent}
          pic={props.pic}
        />
      ) : (
        <MobileBrowser
          handleMouseLeave={handleMouseLeave}
          handleMouseEnter={handleMouseEnter}
          isHovered={isHovered}
          isUnhovered={isUnhovered}
          title={props.title}
          isLoaded={isLoaded}
          onOpen={onOpen}
          onOpenComponent={onOpenComponent}
          pic={props.pic}
        />
      )}
    </div>
  );
};

export default PortoflioTab;
