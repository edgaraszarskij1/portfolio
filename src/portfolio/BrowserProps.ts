export interface PCBrowserProps {
  isHovered: boolean;
  isUnhovered: boolean;
  title: string;
  isLoaded: boolean;
  pic: string;
  onOpenComponent: (
    e: React.MouseEvent<HTMLHeadingElement | SVGElement>
  ) => void;
  handleMouseLeave: (e: React.MouseEvent<HTMLDivElement>) => void;
  handleMouseEnter: (
    e: React.MouseEvent<HTMLDivElement> | React.TouchEvent<HTMLDivElement>
  ) => void;
  onOpen: (e: React.MouseEvent<SVGElement>) => void;
}
