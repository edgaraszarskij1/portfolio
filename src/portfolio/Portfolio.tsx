import React from "react";
import PortfolioTab from "./PortfolioTab";
import "./portfolio.css";
import {
  typing,
  kapinynas,
  tictactoe,
  slidingPuzzle,
  memoryPuzzle,
  brackets,
  armory,
} from "./images";
import SlidingPuzzle from "../projects/sliding puzzle/SlidingPuzzle.jsx";
import Tictactoe from "../projects/tictactoe/Board.jsx";
import MemoryPuzzle from "../projects/memory puzzle/Main.jsx";
import TypingPractise from "../projects/typingPractise/TypingPractise";
import CbaBE from "./CbaBE";
const Portfolio = (): JSX.Element => {
  return (
    <div className={"portfolio-container"}>
      <h1 className={"portfolio-title main-title-large"}>PORTFOLIO</h1>
      <div className={"inner-portfolio"}>
        <PortfolioTab
          pic={tictactoe}
          title={"Tic Tac Toe"}
          Component={Tictactoe}
        />
        <PortfolioTab
          pic={typing}
          title={"Typing Speed Checker"}
          Component={TypingPractise}
        />
        <PortfolioTab
          pic={kapinynas}
          title={"Animals Cemetary System"}
          Component={CbaBE}
        />
        <PortfolioTab
          pic={slidingPuzzle}
          title={"Sliding Puzzle"}
          Component={SlidingPuzzle}
        />
        <PortfolioTab
          pic={memoryPuzzle}
          title={"Memory Puzzle"}
          Component={MemoryPuzzle}
        />
        <PortfolioTab pic={brackets} title={"Brackets"} Component={CbaBE} />
        <PortfolioTab pic={armory} title={"Wow Armory"} Component={CbaBE} />
      </div>
    </div>
  );
};

export default Portfolio;
