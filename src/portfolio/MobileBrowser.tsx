import React from "react";
import "./portfolio.css";
import ZoomInIcon from "@material-ui/icons/ZoomIn";
import OpenInNewIcon from "@material-ui/icons/OpenInNew";
import { PCBrowserProps } from "./BrowserProps";

const MobileBrowser = (props: PCBrowserProps): JSX.Element => {
  return !props.isLoaded ? (
    <div style={{ height: "100%", width: "100%" }}>
      <div className={"box"} onTouchStart={props.handleMouseEnter}>
        <div
          className={
            props.isHovered
              ? "pic-hover"
              : props.isUnhovered
              ? "pic-unhover"
              : ""
          }
        >
          <div className={"center-icon"}>
            <ZoomInIcon
              className={
                props.isHovered
                  ? "icon"
                  : props.isUnhovered
                  ? "icon-unhover"
                  : "hide-icons"
              }
              onClick={props.onOpen}
            />
            <OpenInNewIcon
              className={
                props.isHovered
                  ? "icon"
                  : props.isUnhovered
                  ? "icon-unhover"
                  : "hide-icons"
              }
              onClick={props.onOpenComponent}
            />
          </div>
        </div>
        {!props.isLoaded ? (
          <img src={props.pic} alt={"typing"} className={"small-pic-size"} />
        ) : (
          <div className={"gray-bg"}></div>
        )}
        <h3 className={"port-title"} onClick={props.onOpenComponent}>
          {props.title}
        </h3>
      </div>
    </div>
  ) : (
    <div className={"gray-bg"}></div>
  );
};

export default MobileBrowser;
