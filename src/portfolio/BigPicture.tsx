import React from "react";
import CloseIcon from "@material-ui/icons/Close";
import "./portfolio.css";

interface PictureProps {
  onClose: (e: React.MouseEvent<HTMLDivElement | SVGElement>) => void;
  pic: string;
}

const BigPicture = (props: PictureProps): JSX.Element => {
  return (
    <>
      <div className={"shadow"} onClick={props.onClose}>
        <CloseIcon onClick={props.onClose} className={"close-button"} />
        <img src={props.pic} className="big-pic" alt={"typing"} />
      </div>
    </>
  );
};

export default BigPicture;
