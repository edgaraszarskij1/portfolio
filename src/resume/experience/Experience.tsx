import React from "react";
import "./experience.css";

interface ExperienceProps {
  children?: React.ReactNode;
  title: string;
  startYear: number;
  endYear?: number;
  subTitle: string;
}

const Experience = (props: ExperienceProps): JSX.Element => {
  return (
    <div className={"border-ui"}>
      <h2 className={"second-title resume-text"}>{props.title}</h2>
      <h3 className={"year-of"}>
        {" "}
        {props.startYear} - {props.endYear}{" "}
      </h3>
      <div className={"item-container "}>
        <h3 className={"title resume-text"}>{props.subTitle} </h3>
        <div className={"text-container"}>
          <p className={"text-title-resume resume-text"}>{props.children}</p>
        </div>
      </div>
    </div>
  );
};

export default Experience;
