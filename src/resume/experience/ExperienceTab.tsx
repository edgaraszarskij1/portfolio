import React from "react";
import Experience from "./Experience";
import "./experience.css";

const ExperienceTab = (): JSX.Element => {
  return (
    <div className={"experience-container"}>
      <h2 className={"main-title"}> Working Experience</h2>
      <div>
        <Experience
          title={"Intern"}
          subTitle={"ITSC"}
          startYear={2017}
          endYear={2017}
        >
          Website development using information from Redmine JavaScript, HTML,
          CSS
        </Experience>
        <Experience
          title={"Intern"}
          subTitle={"Vilprint"}
          startYear={2018}
          endYear={2018}
        >
          Upgrading Website JavaScript, HTML, CSS
        </Experience>
        <Experience
          title={"Academy student"}
          subTitle={"TietoEVRY"}
          startYear={2021}
          endYear={2021}
        >
          Web application development
        </Experience>
      </div>
      <h2 className={"main-title"}>Education</h2>
      <div>
        <Experience
          title={"Vilnius Gediminas Technical University"}
          subTitle={"Bachelor of Software Engineering"}
          startYear={2015}
          endYear={2019}
        ></Experience>
      </div>
    </div>
  );
};

export default ExperienceTab;
