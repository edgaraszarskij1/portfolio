import React from "react";
import ExperienceTab from "./experience/ExperienceTab";
import SkillsTab from "./skills/SkillsTab";

const Resume = () => {
  return (
    <div className={"portfolio-container"}>
      <h1 className={"main-title-large center-title"}>RESUME </h1>
      <SkillsTab />
      <ExperienceTab />
    </div>
  );
};

export default Resume;
