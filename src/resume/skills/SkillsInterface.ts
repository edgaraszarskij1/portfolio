export interface SkillsInterface {
  skillTitle: string;
  fullStars: number;
  halfStar: boolean;
  perc?: number;
}
