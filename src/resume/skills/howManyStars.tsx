import React from "react";
import StarIcon from "@material-ui/icons/Star";
import HalfFullColoredStar from "./HalfFullColoredStar";

export const howManyStars = (
  stars: number,
  halfStar: boolean,
  perc?: number
): React.ReactNode => {
  const starsArray: Array<React.ReactNode> = [];
  const fiveStars: number = 5;
  for (let i = 0; i < stars; i++) {
    starsArray.push(<StarIcon className={"star act"} key={i} />);
  }
  if (halfStar) {
    starsArray.push(<HalfFullColoredStar perc={perc} key={4} name="half" />);
  }
  if (starsArray.length !== fiveStars) {
    for (let i = starsArray.length; i < fiveStars; i++) {
      starsArray.push(
        <HalfFullColoredStar
          perc={0}
          key={i + starsArray.length}
          name="empty"
        />
      );
    }
  }
  return starsArray;
};
