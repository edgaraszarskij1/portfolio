import React from "react";
import Skills from "./Skills";
import "./skills.css";
import "../experience/experience.css";

const SkillsTab = (): JSX.Element => {
  return (
    <>
      <h2 className={"skills-title"}>Skills</h2>
      <div className={"skills-container"}>
        <Skills
          skillTitle={"JavaScript"}
          fullStars={3}
          halfStar={true}
          perc={30}
        />
        <Skills skillTitle={"TypeScript"} fullStars={2} halfStar={false} />
        <Skills skillTitle={"React"} fullStars={3} halfStar={true} perc={25} />
        <Skills skillTitle={"HTML"} fullStars={3} halfStar={false} />
        <Skills skillTitle={"CSS"} fullStars={3} halfStar={false} />
      </div>
    </>
  );
};

export default SkillsTab;
