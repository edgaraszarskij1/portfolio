import React from "react";
import SvgIcon from "@material-ui/core/SvgIcon";
import "./halfstar.css";

const HalfFullColoredStar = (props: any) => {
  return (
    <SvgIcon {...props} className={"star-size"}>
      <linearGradient id={props.name}>
        <stop offset={props.perc + "%"} className={"half-star coloring-star"} />
        <stop offset={"0%"} className={"other-half-star"} />
      </linearGradient>
      <path
        d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21z"
        fill={`url(${"#" + props.name})`}
      />
    </SvgIcon>
  );
};

export default HalfFullColoredStar;
