import React from "react";
import { SkillsInterface } from "./SkillsInterface";
import { howManyStars } from "./howManyStars";
import "./skills.css";

const Skills = (props: SkillsInterface): JSX.Element => {
  const { skillTitle, fullStars, halfStar, perc } = props;

  return (
    <div className={"container-star"}>
      <p className={"skills-p"}>{skillTitle}</p>
      <div className={"stars-ui"}>
        {" "}
        {howManyStars(fullStars, halfStar, perc)}
      </div>
    </div>
  );
};

export default Skills;
