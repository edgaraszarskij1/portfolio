import React from "react";
import { me } from "../portfolio/images";
import "../sidebar/screen.css";
import "./about.css";
import "../portfolio/portfolio.css";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

const About = (): JSX.Element => {
  return (
    <div className={"about-container"}>
      <h1 className={"main-title-large "}>ABOUT ME </h1>
      <div className={"top-space"}>
        {me ? (
          <AccountCircleIcon className={"thub-left"} />
        ) : (
          <img src={me} className={"img-left"} alt={"typing"} />
        )}
        <div className={"text-left"}>
          <h1 className={"sub-title-about"}>Hi!</h1>
          <p className={"text"}>
            {" "}
            I am most interested in working with web technologies like React,
            HTML, CSS. During my free time I usually try to increase my
            knowledge in React js library and try to apply it to my personal
            project. I am: curious, creative, punctual perfectionist,
            responsible. Other things that I do during my free time are meeting
            with my friends and playing computer games.
          </p>
          <ul className={"bullets"}>
            <li className={"li-text"}>
              <b className={"li-after-bullets"}>Full name </b>Edgaras Žarskij
            </li>
            <li className={"li-text"}>
              <b className={"li-after-bullets"}>Age</b>24
            </li>
            <li className={"li-text"}>
              <b className={"li-after-bullets"}>Language</b>English, Lithuanian
            </li>
            <li className={"li-text"}>
              <b className={"li-after-bullets"}>Location</b>Vilnius, Lithuania
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default About;
