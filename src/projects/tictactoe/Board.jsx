import React, { Component, Fragment } from "react";
import BoardDrawer from "./boardDrawer";

let howMany = 0;

class Board extends Component {
  constructor(props) {
    super(props);
    const newArr = [];

    for (var i = 0; i < 3; i++) {
      newArr[i] = [];
      for (var j = 0; j < 3; j++) {
        howMany++;
        newArr[i][j] = howMany;
      }
    }

    this.state = { newArr, turns: 0, ended: false, won: "" };
    this.handleClick = this.handleClick.bind(this);
  }

  whoWon = (x) => {
    if (x === 3 && !this.state.ended) {
      this.setState((prevState) => ({ ...prevState, won: "X" }));
    }
    if (x === -3 && !this.state.ended) {
      this.setState((prevState) => ({ ...prevState, won: "0" }));
    }
  };

  newGameHandler = () => {
    const newArr = this.state.newArr;
    howMany = 0;
    for (var i = 0; i < 3; i++) {
      newArr[i] = [];
      for (var j = 0; j < 3; j++) {
        howMany++;
        newArr[i][j] = howMany;
      }
    }
    this.setState({ newArr, turns: 0, ended: false });
  };

  isEnded = (arr) => {
    for (let i = 0; i < arr.length; i++) {
      if (
        arr[0][i] === arr[1][i] &&
        arr[1][i] === arr[2][i] &&
        arr[0][i] === arr[2][i]
      ) {
        if (!this.state.ended) {
          this.setState((prevState) => ({
            ...prevState,
            ended: true,
            won: arr[0][i],
          }));
        }
      }
      if (
        arr[i][0] === arr[i][1] &&
        arr[i][1] === arr[i][2] &&
        arr[i][0] === arr[i][2]
      ) {
        if (!this.state.ended) {
          this.setState((prevState) => ({
            ...prevState,
            ended: true,
            won: arr[i][0],
          }));
        }
      }
    }
  };

  isDiagonal = (arr) => {
    let x = 0;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i][i] !== "X" && arr[i][i] !== "O") break;
      x += arr[i][i] === "X" ? 1 : -1;
    }
    this.whoWon(x);
    return Math.abs(x) === 3;
  };

  isDiagonal1 = (arr) => {
    let x = 0;
    for (let i = 0; i < arr.length; i++) {
      if (
        arr[i][arr.length - i - 1] !== "X" &&
        arr[i][arr.length - i - 1] !== "O"
      )
        break;
      x += arr[i][arr.length - i - 1] === "X" ? 1 : -1;
    }
    this.whoWon(x);
    return Math.abs(x) === 3;
  };
  componentDidUpdate() {
    if (
      this.isDiagonal1(this.state.newArr) ||
      this.isDiagonal(this.state.newArr) ||
      this.isEnded(this.state.newArr)
    ) {
      if (!this.state.ended) {
        this.setState((prevState) => ({ ...prevState, ended: true }));
      }
      return;
    }
  }
  handleClick = (boardCell) => {
    if (!this.state.ended) {
      if (this.state.turns % 2 === 0) {
        this.setState({
          newArr: this.state.newArr.map((ro) =>
            ro.map((el) => (el === boardCell ? "X" : el))
          ),
          turns: this.state.turns + 1,
        });
      }
      if (this.state.turns % 2 !== 0) {
        this.setState({
          newArr: this.state.newArr.map((ro) =>
            ro.map((el) => (el === boardCell ? "O" : el))
          ),
          turns: this.state.turns + 1,
        });
      }
    }
  };

  render() {
    return (
      <Fragment>
        {this.state.ended ? (
          <div className={"won"}>Won: {this.state.won}</div>
        ) : (
          <div className={"won"}>
            Next:{" "}
            {this.state.turns % 2 === 0
              ? "X"
              : this.state.turns === 9
              ? "No more turns left"
              : "O"}
          </div>
        )}
        <BoardDrawer
          tiles={this.state.newArr}
          turn={this.state.turns}
          handleClick={this.handleClick}
        />
        <button
          onClick={() => this.newGameHandler()}
          className={"new-game-button"}
        >
          New Game
        </button>
      </Fragment>
    );
  }
}

export default Board;
