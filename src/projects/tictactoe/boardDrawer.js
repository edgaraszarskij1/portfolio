import React from "react";
import "./game.css";

function boardDrawer(props) {
  return (
    <div className="boxs">
      {props.tiles.map((boardRow, row) =>
        boardRow.map((boardCell, cell) =>
          boardCell === "X" ? (
            <div className="tile" key={cell}>
              <div className="cross"></div>
            </div>
          ) : boardCell === "O" ? (
            <div className="tile" key={cell}>
              <div className="circle"></div>
            </div>
          ) : props.turn % 2 === 0 ? (
            <div
              className="tile"
              id="test1"
              key={cell}
              onClick={() => props.handleClick(boardCell)}
            ></div>
          ) : props.turn % 2 !== 0 ? (
            <div
              className="tile"
              id="test"
              key={cell}
              onClick={() => props.handleClick(boardCell)}
            ></div>
          ) : undefined
        )
      )}
    </div>
  );
}

export default boardDrawer;
