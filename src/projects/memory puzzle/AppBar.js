import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    height: "10% !important",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  appbar: {
    alignItems: "center",
  },
});

const Layout = (props) => {
  const { classes, children } = props;
  return (
    <div className={"memory-header-ui"}>
      <AppBar className={classes.appbar} position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Memory Puzzle
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={"max-height-width"}>{children}</div>
    </div>
  );
};

Layout.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Layout);
