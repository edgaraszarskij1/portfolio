import React, { Component } from "react";
import Puzzle from "./Puzzle";
import Layout from "./AppBar";

class Main extends Component {
  render() {
    return (
      <div className={"mpuzzle-container"}>
        <Layout classNmae={"memory-header-ui"}></Layout>
        <Puzzle />
      </div>
    );
  }
}

export default Main;
