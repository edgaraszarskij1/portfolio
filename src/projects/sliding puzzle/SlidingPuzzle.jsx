import React, { Component } from "react";
import Card from "./Card.jsx";
import { shuffle } from "./shuffle";

class SlidingPuzzle extends Component {
  constructor(props) {
    super(props);
    const newArr = [];
    for (let i = 0; i < 9; i++) {
      newArr.push(i);
    }
    shuffle(newArr);
    this.state = { newArr, isFinished: false };
  }

  newGameHandler = () => {
    const newArr = this.state.newArr;
    this.setState({ newArr: shuffle(newArr), isFinished: false });
  };

  isSorted = (array) => {
    for (let i = 0; i < array.length - 1; i++) {
      if (array[i] > array[i + 1]) {
        return false;
      }
    }
    return true;
  };

  handleClick = (id) => {
    const tileIndex = this.state.newArr.findIndex((tile) => tile === id);
    const clonedTilesArray = [...this.state.newArr];
    const tileZeroIndex = clonedTilesArray.indexOf(0);
    const size = 3;
    if (this.isSorted(clonedTilesArray)) {
      this.setState({ isFinished: true });
      return;
    }

    if (
      (tileIndex + 1 === tileZeroIndex &&
        (tileIndex + 1) % size > tileIndex % size) ||
      (tileIndex - 1 === tileZeroIndex &&
        (tileIndex - 1) % size < tileIndex % size) ||
      tileIndex + size === tileZeroIndex ||
      tileIndex - size === tileZeroIndex
    ) {
      const temp = clonedTilesArray[tileIndex];
      clonedTilesArray[tileIndex] = clonedTilesArray[tileZeroIndex];
      clonedTilesArray[tileZeroIndex] = temp;
      this.setState({ newArr: clonedTilesArray });
    }
  };

  render() {
    return (
      <div className={"sliding-puzzle-container"}>
        {!this.isSorted(this.state.newArr) ? (
          <>
            <div className="finished">New Game</div>
            <Card newArr={this.state.newArr} handleClick={this.handleClick} />
          </>
        ) : (
          <>
            <div className="finished">Finished</div>
            <Card newArr={this.state.newArr} handleClick={this.handleClick} />
          </>
        )}
        <button onClick={this.newGameHandler} className={"new-game-button-sp"}>
          New Game
        </button>
      </div>
    );
  }
}

export default SlidingPuzzle;
