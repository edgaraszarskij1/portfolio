import React from "react";
import "../typingPractise.css";
import { ResultProperties } from "../ResultProperties";

const DisplayResults = (props: {
  result: Array<ResultProperties>;
}): JSX.Element => {
  const everyFifthResult: number = 5;

  const result = (element: ResultProperties): string => {
    if (element.time === "00:00:000") {
      return (
        "Solved in: Unsolved " +
        element.word +
        " Remaining time: " +
        element.time
      );
    } else {
      return (
        "Solved in: " +
        element.remainingTime +
        " Word: " +
        element.word +
        " Remaining time: " +
        element.time
      );
    }
  };

  return (
    <div className={"display-results"}>
      {props.result
        .slice(
          props.result.length - props.result.length - everyFifthResult,
          props.result.length
        )
        .map((element) => (
          <p key={element.time} className={"result-items"}>
            {result(element)}
          </p>
        ))}
    </div>
  );
};

export default DisplayResults;
